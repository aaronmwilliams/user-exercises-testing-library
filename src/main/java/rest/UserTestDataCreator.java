package rest;

import exercise.ExerciseBuilder;
import user.User;
import user.UserBuilder;

public class UserTestDataCreator extends AbstractBaseRest {

    private UserBuilder userBuilder = new UserBuilder();
    private ExerciseBuilder exerciseBuilder = new ExerciseBuilder();

    public User createUser() {
        entity = buildEntity(toJson(userBuilder.buildRandomUser()));
        return gson.fromJson(post(USERS_PATH, entity).getBody(), User.class);
    }

    public User createUser(String name, String postcode) {
        entity = buildEntity(toJson(new User(name, postcode)));
        return gson.fromJson(post(USERS_PATH, entity).getBody(), User.class);
    }

}
