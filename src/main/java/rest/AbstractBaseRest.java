package rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractBaseRest {

    protected static String BASE_URL = "http://localhost:{SERVER_PORT}/api";
    protected RestTemplate restTemplate = new RestTemplate();
    protected HttpHeaders httpHeaders = new HttpHeaders();
    protected GsonBuilder builder = new GsonBuilder();
    protected Gson gson = builder.create();

    protected static final String USERS_PATH = "/users";
    protected static final String EXERCISES_PATH = "/exercises";
    protected static final String LOG_PATH = "/log";

    protected HttpEntity<String> entity;

    public AbstractBaseRest() {
        buildHeaders()
                .buildBaseURL();
    }

    private AbstractBaseRest buildHeaders() {
        httpHeaders.set("Content-Type", "application/json");
        return this;
    }

    private AbstractBaseRest buildBaseURL() {
        String serverPortArgument = System.getProperty("server.port");
        if (serverPortArgument == null) {
            BASE_URL = BASE_URL.replace("{SERVER_PORT}", "8080");
        } else {
            BASE_URL = BASE_URL.replace("{SERVER_PORT}", serverPortArgument);
        }
        return this;
    }

    protected ResponseEntity<String> post(String path, HttpEntity<String> entity) {
        return restTemplate
                .exchange(BASE_URL + path,
                        HttpMethod.POST,
                        entity,
                        String.class);
    }

    protected HttpEntity<String> buildEntity(String string) {
        return new HttpEntity<>(string, httpHeaders);
    }

    protected <T> String toJson(T type) {
        return gson.toJson(type);
    }

}
