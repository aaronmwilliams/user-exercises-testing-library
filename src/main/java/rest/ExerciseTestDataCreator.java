package rest;

import exercise.Exercise;
import exercise.ExerciseBuilder;
import org.springframework.http.HttpEntity;

public class ExerciseTestDataCreator extends AbstractBaseRest {

    private ExerciseBuilder exerciseBuilder = new ExerciseBuilder();
    HttpEntity<String> entity;

    public Exercise createExercise() {
        entity = buildEntity(toJson(exerciseBuilder.buildRandomExercise()));
        return gson.fromJson(post(EXERCISES_PATH, entity).getBody(), Exercise.class);
    }

    public Exercise createExercise(String description) {
        entity = buildEntity(toJson(new Exercise(description)));
        return gson.fromJson(post(EXERCISES_PATH, entity).getBody(), Exercise.class);
    }

}
