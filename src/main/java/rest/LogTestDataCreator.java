package rest;

import exercise.Exercise;
import log.Log;
import user.User;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogTestDataCreator extends AbstractBaseRest {

    private String logDate;
    private Date today = new Date();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public LogTestDataCreator() {
        this.logDate = simpleDateFormat.format(today);
    }

    public Log createLog() {
        UserTestDataCreator userTestDataCreator = new UserTestDataCreator();
        ExerciseTestDataCreator exerciseTestDataCreator = new ExerciseTestDataCreator();

        User user = userTestDataCreator.createUser();
        Exercise exercise = exerciseTestDataCreator.createExercise();
        Log log = new Log(user.getId(), user.getName(),
                            exercise.getId(), exercise.getDescription(), getLogDate());

        entity = buildEntity(toJson(log));
        return gson.fromJson(post(LOG_PATH, entity).getBody(), Log.class);
    }

    public String getLogDate() {
        return logDate;
    }

}
