package exercise;

import org.apache.commons.lang3.RandomStringUtils;

public class ExerciseBuilder {

    public Exercise buildRandomExercise() {
        return new Exercise(createRandomDescription());
    }

    public Exercise buildExercise(String description) {
        return new Exercise(description);
    }

    public Exercise buildExerciseWithTooLongDescription() {
        return new Exercise(RandomStringUtils.randomAlphabetic(256));
    }

    private String createRandomDescription() {
        return RandomStringUtils.randomAlphabetic(255);
    }

}
