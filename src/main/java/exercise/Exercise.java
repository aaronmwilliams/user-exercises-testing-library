package exercise;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public @Data class Exercise {

    private @Getter @Setter int id;
    private @Getter @Setter String description;

    public Exercise(String description) {
        this.description = description;
    }

}