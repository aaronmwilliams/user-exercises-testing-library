package log;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public @Data class Log {

    private @Getter @Setter  int id;
    private @Getter @Setter int userId;
    private @Getter @Setter String userName;
    private @Getter @Setter int exerciseId;
    private @Getter @Setter String description;
    private @Getter @Setter String date;

    public Log(int userId, String userName, int exerciseId,
               String description, String date) {
        this.userId = userId;
        this.userName = userName;
        this.exerciseId = exerciseId;
        this.description = description;
        this.date = date;
    }

}
