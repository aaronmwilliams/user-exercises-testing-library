package log;

import exercise.Exercise;
import user.User;

public class LogBuilder {

    public Log buildLog(User user, Exercise exercise, String date ) {
        return new Log(user.getId(), user.getName(),
                exercise.getId(), exercise.getDescription(), date);
    }

}
