package user;

import org.apache.commons.lang3.RandomStringUtils;

public class UserBuilder {

    public User buildRandomUser() {
        return new User(createRandomName(), createRandomPostcode());
    }

    public User buildUserWithTooLongName() {
        return new User(RandomStringUtils.randomAlphabetic(36), createRandomPostcode());
    }

    public User buildUserWithTooLongPostcode() {
        return new User(createRandomName(), RandomStringUtils.randomAlphabetic(9));
    }

    private String createRandomName() {
        return RandomStringUtils.randomAlphabetic(35);
    }

    private String createRandomPostcode() {
        return RandomStringUtils.random(8, true, true);
    }

}
