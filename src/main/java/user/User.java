package user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public @Data class User {

    private @Getter @Setter int id;
    private @Getter @Setter String name;
    private @Getter @Setter String postcode;

    public User (String name, String postcode) {
        this.name = name;
        this.postcode = postcode;
    }

    public User (int id, String name, String postcode) {
        this.id = id;
        this.name = name;
        this.postcode = postcode;
    }

}