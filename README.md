# user-exercises-testing-library

### Purpose of project
Test support library used to create test data and support testing for frameworks which test user-exercises-rest.

### Add as dependency 

This project is published to maven central and can be downloaded as a dependency.

*Maven*

```<dependency>
<groupId>org.bitbucket.aaronmwilliams</groupId>
<artifactId>user-exercises-testing-library</artifactId>
<version>1.0.0</version>
<type>pom</type>
</dependency>
```

*Gradle*

`compile 'org.bitbucket.aaronmwilliams:user-exercises-testing-library:1.0.0'`

### Installing and Publishing to Local Maven

Import as a *GRADLE* project. 

First build the project: `./gradlew clean build`

Then public the build to your maven local .m2 directory: `./gradlew install`

The library can now be used by other projects in this repository. 

### Publishing to Maven Central

Note you will need the Sonatype username and password added as system properties: `sonatype.username` & `sonatype.password`

If there is a new version to publish make sure you have updated the publishing version number correctly.

Generate a new POM file by running gradle task `generatePomFileForMavenJavaPublication`

Publish the build to Sonatype by rynning gradle task `publish`.

Login to https://oss.sonatype.org and check the artifacts are published okay if so close, release and drop the staged repository.